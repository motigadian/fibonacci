
import timeit
import Tkinter
import turtle


def iterative_fib():
    a, b = 1, 1
    while 1:
        yield a
        a, b = b, a + b


def plot_fib_spiral(n):
    fib = iterative_fib()
    fibo_nr = [fib.next() for i in range(n)]  # Fibonacci numbers this could be calculated instead
    t = turtle.Turtle()

    def draw_square(side_length):  # Function for drawing a square
        for i in range(4):
            t.forward(side_length)
            t.right(90)

    nr_squares = len(fibo_nr)

    factor = 3  # Enlargement factor
    t.penup()
    t.goto(50, 50)  # Move starting point right and up
    t.pendown()
    for i in range(nr_squares):
        draw_square(factor * fibo_nr[i])  # Draw square
        t.penup()  # Move to new corner as starting point
        t.forward(factor * fibo_nr[i])
        t.right(90)
        t.forward(factor * fibo_nr[i])
        t.pendown()

    t.penup()
    t.goto(50, 50)  # Move to starting point
    t.setheading(0)  # Face the turtle to the right
    t.pencolor('red')
    t.pensize(3)
    t.pendown()
    # Draw quartercircles with fibonacci numbers as radius
    for i in range(nr_squares):
        t.circle(-factor * fibo_nr[i], 90)  # minus sign to draw clockwise


def test(n):
    fib = iterative_fib()
    for i in range(1,n):
        print (fib.next())

#test(10)
if __name__ == '__main__':

    # print timeit.timeit('test(n)',setup="from __main__ import test;n=10")
    # print
    # print timeit.timeit('test(n)',setup="from __main__ import test;n=100")
    # print
    # print timeit.timeit('test(n)',setup="from __main__ import test;n=100")
    N = int(input("enter number of values:"))
    plot_fib_spiral(N)